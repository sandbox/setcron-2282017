<?php
$dir = __DIR__;
$dir_parts = explode('/',$dir);
array_pop($dir_parts);
$module_dir = implode('/', $dir_parts).'/';

require_once $module_dir.'packages/anlutro/cURL/cURL.php';
require_once $module_dir.'packages/anlutro/cURL/Request.php';
require_once $module_dir.'packages/anlutro/cURL/Response.php';

const SetCronAPI_URL = 'https://www.setcron.com/api/server/';

function get_plugin_notification($type = 'joomla3x'){
    $curl = new anlutro\cURL\cURL;

    $url = 'https://www.setcron.com/notification/plugin/'.$type;
    return $curl->newRequest('get', $url)
            ->setHeader('Accept', 'html')->send();
}

function get_tasks($apikey,$sort = null,$direction = null){
    $curl = new anlutro\cURL\cURL;

    $url = SetCronAPI_URL.'tasks?sort='.$sort.'&direction='.$direction;
    return $curl->newRequest('get', $url)
            ->setHeader('apikey', $apikey)
            ->setHeader('content-type', 'application/json')
            ->setHeader('Accept', 'json')->send();
}


function get_task($apikey, $id){
    $curl = new anlutro\cURL\cURL;

    $url = SetCronAPI_URL.'task?id='.$id;
    return $curl->newRequest('get', $url)
            ->setHeader('apikey', $apikey)
            ->setHeader('content-type', 'application/json')
            ->setHeader('Accept', 'json')->send();
}

function save_task($apikey, $data){
    
    if(isset($data['id']) && $data['id'] != 0){
        $id = $data['id'];
        $curl = new anlutro\cURL\cURL;
        $url = SetCronAPI_URL.'task?id='.$id;
        return $curl->newRequest('put', $url, $data)
                ->setHeader('apikey', $apikey)
                ->setHeader('Accept', 'application/json')->send();

    } else {
        $curl = new anlutro\cURL\cURL;

        $url = SetCronAPI_URL.'task';
        return $curl->newRequest('post', $url, $data)
                ->setHeader('apikey', $apikey)
                ->setHeader('Accept', 'application/json')->send();
    }
    
}

function delete_task($apikey, $id) {
    $curl = new anlutro\cURL\cURL;

        $url = SetCronAPI_URL.'task?id='.$id;
        return $curl->newRequest('delete', $url)
                ->setHeader('apikey', $apikey)
                ->setHeader('Accept', 'application/json')->send();
}

function organize_tasks($apikey, $data) {
    $curl = new anlutro\cURL\cURL;

        $url = SetCronAPI_URL.'tasks_organize';
        return $curl->newRequest('post', $url, $data)
                ->setHeader('apikey', $apikey)
                ->setHeader('Accept', 'application/json')->send();
}

function get_timezones($apikey) {
    $curl = new anlutro\cURL\cURL;

        $url = SetCronAPI_URL.'timezones';
        return $curl->newRequest('get', $url)
                ->setHeader('apikey', $apikey)
                ->setHeader('Accept', 'application/json')->send();
}

function get_task_usage($apikey, $id) {
    $curl = new anlutro\cURL\cURL;

        $url = SetCronAPI_URL.'usage?id='.$id;
        return $curl->newRequest('get', $url)
                ->setHeader('apikey', $apikey)
                ->setHeader('Accept', 'application/json')->send();
}